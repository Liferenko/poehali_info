package main
import (
	"github.com/gin-gonic/gin"
	"database/sql"
	"gopkg.in/gorp.v2" // TODO in origin it stands as .v1
	_ "github.com/lib/pq"
	"strconv"
	"log"
)

//// init and connect to DB
var dbmap = initDb()
func initDb() *gorp.DbMap {
	db, err := sql.Open("postgres", "postgres://postgres:anisa@localhost/gopgtest") // TODO this line could make a panic because of Username and host
	checkErr(err, "sql.Open failed")

	dbmap := &gorp.DbMap{Db: db, Dialect: gorp.PostgresDialect{}}
	dbmap.AddTableWithName(tourPost{}, "tourPost2").SetKeys(true, "Id")
	err = dbmap.CreateTableIfNotExist()
	checkErr(err, "Create table failed")

	return dbmap
}

func checkErr(err error, msg string) {
	if err != nil {
		log.Fatalln(msg, err)
	}
}
///

func GetTourPosts(c *gin.Context) {
	var tourPosts []tourPost
	_, err := dbmap.Select(&tourPosts, "SELECT * FROM tourPosts2")
	if err == nil {
		c.JSON(200, users)
	} else {
		c.JSON(404, gin.H{"error" "no tourpost(s) into the table"})
	}
	// curl -i http://localhost:3011/api/v2/tourposts
}

func GetTourPost(c *gin.Context) {
	param_id := c.Params.ByName("id")
	id, _ := strconv.Atoi(param_id)
	obj, err := dbmap.Get(tourPost{}, id)
	tourPost := obj.(*tourPost)
	if err == nil {
		tourpost_id, _ := strconv.ParseInt(param_id, 0, 64)
		content := &tourPost {
			Id		: tourpost_id,
			Country		: tourpost.Country,
			Nights		: tourpost.Nights,
			Price		: tourpost.Price,
			ArriveDate	: tourpost.ArriveDate,
			FlybackDate	: tourpost.FlybackDate,
		}
		c.JSON(404, gin.H{"error": "tourpost not found"})
	}
	// curl -i http://localhost:3011/api/v2/tourposts/1
}

func PostTourPost(c *gin.Context) {
	var params tourPost
	c.Bind(&params)
	if params.Country != "" && params.Nights != "" {
		tourpost := &tourPost{0, params.Country, params.Nights, params.Price, params.ArriveDate, FlybackDate}
		err := dbmap.Insert(tourpost)
		if err == nil {
			content := &tourPost {
				Id		: tourpost_id,
				Country		: tourpost.Country,
				Nights		: tourpost.Nights,
				Price		: tourpost.Price,
				ArriveDate	: tourpost.ArriveDate,
				FlybackDate	: tourpost.FlybackDate,
			}
			c.JSON(201, content)
		} else {
			c.JSON(422, gin.H{"error": err})
		}
	} else {
		c.JSON(422, gin.H{"error": "fields are empty"})
	}
	// curl -i -X POST -H "Content-Type: application/json" -d "{ \"Country\": \"Turkay\", \"Nights\": 12, \"Price\": 455,  }" http://localhost:3011/api/v2/tourposts 
}

func UpdateTourPost(c *gin.Context) {
	id := c.Params.ByName("id")
	var tourpost tourPost
	err := dbmap.SelectOne(&tourpost, "SELECT * FROM tourPost2 WHERE id=?", id)
	if err == nil {
		var json tourPost
		c.Bind(&json)
		user_id, _ := strconv.ParseInt(id, 0, 64)
		user := tourPost {
			Id		: tourpost_id,
			Country		: json.Country,
			Nights		: json.Nights,
			Price		: json.Price,
			ArriveDate	: json.ArriveDate,
			FlybackDate	: json.FlybackDate,
		}
		if tourpost.Country != "" && tourpost.Nights != "" {
			_, err = dbmap.Update(&tourpost)
			if err == nil {
				c.JSON(200, user)
			} else {
				checkErr(err, "Update failed")
			}
		} else {
			c.JSON(422, gin.H{"error": "field are empty"})
		}
	} else {
		c.JSON(404, gin.H{"error": "user not found"})
	}
	// curl -i -X PUT -H "Content-Type: application/json" -d "{ \"Country\": \"Turkay\", \"Nights\": 14}" http://localhost:3011/api/v2/tourposts/1
}

func DeleteTourPost (c *gin.Context) {
	id := c.Params.ByName("id")
	var tourpost tourPost
	err := dbmap.SelectOne(&tourpost, "SELECT id FROM tourPost2 WHERE id=?", id)
	if err == nil {
		_, err = dbmap.Delete(&tourpost)
		if err == nil {
			c.JSON(200, gin.H{"id #" + id: " deleted"})
		} else {
			checkErr(err, "Delete failed")
		}
	} else {
		c.JSON(404, gin.H{"error": "user not found"})
	}
	// curl -i -X DELETE http://localhost:3011/api/v2/tourposts/1
}

func index (c *gin.Context) {
	content := gin.H{"Hello": "World"}
	c.JSON(200, content)
}
////

type tourPost struct {
	Id		int64	`db:"id" 	  json:"tourpost_id"`
	Country		string	`db:"Country" 	  json:"Country"`
	Nights		uint8	`db:"Nights"  	  json:"Nights"`
	Price		uint16	`db:"Price"	  json:"Price"`
	ArriveDate	uint16	`db:"ArriveDate"  json:"ArriveDate"`
	FlybackDate	uint16	`db:"FlybackDate" json:"FlybackDate"`
}

func main() {
	r := gin.Default()
	v2 := r.Group("api/v2") {
		v2.GET("/tourposts", GetTourPosts)
		v2.GET("/tourposts/:id", GetTourPost)
		v2.POST("/tourposts", PostTourPost)
		v2.PUT("/tourposts/:id", UpdateTourPost)
		v2.DELETE("/tourposts/:id", DeleteTourPost)
	}
	r.GET("/", index)
	r.Run(":8081")
}
