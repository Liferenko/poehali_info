package main

/* 
Maintainer: Paul Liferenko

TODO:
    - to change func main() to a correct name func connect_to_mysql() 
    - to import this module to real main.go

*/



import (
    "database/sql"
    "fmt"

    _ "github.com/go-sql-driver/mysql"
)


func main() {
    fmt.Println("Go is connecting to mysql...")
    db, err := sql.Open("mysql", "poehaliuser:poehalipass@(127.0.0.1:3306)/poehali_mock")

    if err !=nil{
        panic(err.Error())
    }
    fmt.Println("Connected")
    defer db.Close()

    addNewEmployee, err := db.Prepare("INSERT INTO MOCK_DATA VALUES( ?, ? )")
    if err !=nil{
        panic(err.Error())
    }
    fmt.Println("Inserting OK")
    defer addNewEmployee.Close()


    showEmployee, err := db.Prepare("SELECT id, first_name FROM MOCK_DATA WHERE id = ?")
    if err != nil {
        panic(err.Error())
    }
    defer showEmployee.Close()


// Insert a new employee
//addNewEmployee.Exec(i, (i*10))

// Show employee
showEmployee.QueryRow(1)
}
