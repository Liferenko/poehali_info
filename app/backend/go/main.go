/*
Maintainer: Pavel Liferenko <t.me/Liferenko>
Description: RESTful API for app.
TODO:
  - to add params from this source - https://github.com/gin-gonic/gin#querystring-parameters

*/


package main

import (
  "fmt"
  "net/http"
  "strconv"
  "io/ioutil"
  "encoding/json"

  "github.com/gin-gonic/contrib/static"
  "github.com/gin-gonic/gin"
)

type Response struct {
  Message string `json:"message"`
}

type Employee struct {
    ID          int     `json:"id" binding:"required"`
    FirstName   string  `json:"firstname"`
    SecondName  string  `json:"secondname"`
    CatchPhrase string  `json:"catchPhrase"`
    City        string  `json:"city"`
    AvatarUrl   string  `json:"avatarUrl"`
    Likes       int     `json:"likes"`
}

/** Generates JSON of employees : TODO: to replace to own file */
var employees = []Employee{
  Employee{1, "Аниса", "Сафи", "Did you hear about the restaurant on the moon? Great food, no atmosphere.", "Киев", "http://placekitten.com/100/100",  0},
  Employee{2, "Юлия", "Чупрун", "What do you call a fake noodle? An Impasta.", "Киев", "http://placekitten.com/100/100", 0},
  Employee{3, "Наташа", "Фамилия", "How many apples grow on a tree? All of them.", "Красноярск", "http://placekitten.com/100/100", 0},
  Employee{4, "Света", "Лобода", "Want to hear a joke about paper? Nevermind it's tearable.", "Львов", "http://placekitten.com/100/100", 0},
  Employee{5, "Наташа", "Ростова", "Want to hear a joke about paper? Nevermind it's tearable.", "Вена", "http://placekitten.com/100/100", 0},
  Employee{4, "Евгения", "Лиференко", "Want to hear a joke about paper? Nevermind it's tearable.", "Ростов", "http://placekitten.com/100/100", 0},
}


// Full tourpost json
type TourPost   struct {
  ID                        uint64        `json:"id" binding:"required"`
  Price                     uint32        `json:"price"`
  Nights                    uint8         `json:"nights"`
  FlyFrom                   string        `json:"flyFrom"`
  DepartureDate             string        `json:"departureDate"`
  PostURL                   string        `json:"postUrl"`
  Description               string        `json:"description"`
  CountryName               string        `json:"name"`
  CountryCity               string        `json:"city"`
  ImageUrl                  string        `json:"imageUrl"`
  // ImageAlt               string        `json:"alt"`
  CurrencyDollar            string        `json:"USD"`
  // CurrencyEuro           string        `json:"EUR"`
  // CurrencyUAH            string        `json:"грн"`
  // CurrencyRUB            string        `json:"руб"`
  KeywordsKey1              string        `json:"keywordsKey1"`
  KeywordsKey2              string        `json:"keywordsKey2"`
  KeywordsKey3              string        `json:"keywordsKey3"`

}

func Cors() gin.HandlerFunc {
  return func(c *gin.Context) {
    c.Writer.Header().Add("Access-Control-Allow-Origin", "*")
    c.Next()
  }
}

func main() {
  // Set the router as the default one shipped with Gin
  router := gin.Default()

  router.Use(Cors())
  // Serve the frontend
  router.Use(static.Serve("/", static.LocalFile("../public/", true)))

  api := router.Group("/api")
  {
    api.GET("/", func(c *gin.Context) {
      c.JSON(http.StatusOK, gin.H{
        "message": "pong",
      })
    })
    api.GET("/employees", EmployeeHandler)
    api.GET("/tours", TourHandler)
    api.POST("/employees/like/:jokeID", LikeJoke)
  }
  // Start the app
  router.Run(":3011")
}


// JokeHandler returns a list of employees available (in memory)
func EmployeeHandler(c *gin.Context) {
  c.Header("Content-Type", "application/json")
  c.JSON(http.StatusOK, employees)
}

func TourHandler(c *gin.Context) {
content, err := ioutil.ReadFile("../../databases/mock_data/tours_30_items.json")
    if err != nil {
        fmt.Println(err.Error())
    }


  var tourPosts []TourPost
// json.Unmarshal(content, &tourPosts)
  err2 := json.Unmarshal(content, &tourPosts)
  if err2 != nil {
    fmt.Println("Error JSON Unmarshalling")
    fmt.Println(err2.Error())
  }
  for _,x := range tourPosts{
    fmt.Printf("%s \n",x.Price)
  }


  c.Header("Content-Type", "application/json")
  c.JSON(http.StatusOK, tourPosts)
}

func LikeJoke(c *gin.Context) {
  // Check Employee ID is valid
  if jokeid, err := strconv.Atoi(c.Param("jokeID")); err == nil {
    // find Employee and increment likes
    for i := 0; i < len(employees); i++ {
      if employees[i].ID == jokeid {
        employees[i].Likes = employees[i].Likes + 1
      }
    }
    c.JSON(http.StatusOK, &employees)
  } else {
    // the employees ID is invalid
    c.AbortWithStatus(http.StatusNotFound)
  }
}
