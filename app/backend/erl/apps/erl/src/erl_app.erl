%%%-------------------------------------------------------------------
%% @doc erl public API
%% @end
%%%-------------------------------------------------------------------

-module(erl_app).

-behaviour(application).

-export([start/2, stop/1]).

start(_StartType, _StartArgs) ->
    erl_sup:start_link().

stop(_State) ->
    ok.

%% internal functions
