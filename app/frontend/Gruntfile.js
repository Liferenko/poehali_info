// брал эти файлы из https://github.com/gruntjs/grunt-contrib-sass

module.exports = function(grunt) {

grunt.initConfig({
  sass: {
    dist: {
      files: [{
        expand: true,
        cwd: './scss',  //
        src: ['*.scss', '*.sass'],
        dest: './src/static/css',
        ext: '.css'
      }]
    }
  },
  watch: {
    files: ['**/*.scss'],
    tasks: ['sass'],
  },
});


grunt.loadNpmTasks('grunt-contrib-sass');
grunt.loadNpmTasks('grunt-contrib-watch');

grunt.registerTask('default', ['sass']);
grunt.registerTask('run', ['watch']);
};
