import { combineReducers } 		from 'redux';
import countReducer 			from './countReducer';
import addToFavoriteReducer 	        from './addToFavoriteReducer';


export default combineReducers({
	count: countReducer,
	activeFavStar: addToFavoriteReducer,	
})
