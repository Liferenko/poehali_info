import { 
	ADD_TO_FAVORITE,
} from '../actions/types'

export default function addToFavoriteReducer(state = 0, action) {
	console.log(`action from addToFavoriteReducer - ${action.type}`)
	console.log(`state from addToFavoriteReducer - ${state}`)
	switch (action.type) {
		case ADD_TO_FAVORITE: {
			return !state;
		}
		default: {
			return state;
		}
	}
}
