export const INCREMENT_COUNT 		= 'INCREMENT_COUNT';
export const DEINCREMENT_COUNT 		= 'DEINCREMENT_COUNT';
export const SET_VALUE 			= 'SET_VALUE';


// Favorite star
export const ADD_TO_FAVORITE		= 'ADD_TO_FAVORITE';
export const UNDO_FAVORITE		= 'UNDO_FAVORITE';
