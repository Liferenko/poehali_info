import React, { Component } from 'react';
import styles from './App.css';
import { Switch, Route } from 'react-router-dom';

// Components
// import Counter from './components/Counter';
// /Components

// Containets
import Navigation from './containers/Navigation'
import Footer from './containers/Footer';
// /Containers


// Pages
import Home from './pages/Home';
import Liked from './pages/Liked';
import Contacts from './pages/Contacts';
// /Pages




class App extends Component {
  render() {
    return (
    <div className={styles.App}>
        <Navigation />
        
        <Switch>
            <Route exact path="/" component={Home} />
            <Route path="/liked" component={Liked} />
            <Route path="/contacts" component={Contacts} />
        </Switch>

        <Footer />
    </div>
    );
  }
}

export default App;
