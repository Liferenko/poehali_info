import React, {Component} from 'react';
import {
	  Collapse,
	  Navbar,
	  NavbarToggler,
	  NavbarBrand,
	  Nav,
	  NavItem,
	  NavLink,
          UncontrolledDropdown,
	  DropdownToggle,
	  DropdownMenu,
	  DropdownItem,
	  Media,
} from 'reactstrap';
import { Link } from 'react-router-dom';



const navData = {
	logo: {
		url: 'https://cdn2.iconfinder.com/data/icons/geest-travel-kit/128/travel_journey-15-128.png',
		alt: 'Поехали точка инфо - ресурс подбора туров.',
	}
}



class Navigation extends Component {
  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.state = {
        navData: navData,
      isOpen: false,
    };
  }
  toggle() {
    this.setState({
      isOpen: !this.state.isOpen
    });
  }


  render() {
    return (
      <div>
        <Navbar color="light" light expand="md">
          <NavbarBrand href="/"><Media data-src={this.state.navData.logo.url} alt={this.state.navData.logo.alt}></Media></NavbarBrand>
          <NavbarToggler onClick={this.toggle} />
          <Collapse isOpen={this.state.isOpen} navbar>
            <Nav className="ml-auto" navbar>
              <NavItem>
                <NavLink><Link to="/">Main</Link></NavLink>
              </NavItem>
              <NavItem>
                <NavLink><Link to="/liked">Liked</Link></NavLink>
              </NavItem>
              <NavItem>
                <NavLink><Link to="/contacts">Contacts</Link></NavLink>
              </NavItem>	
              <UncontrolledDropdown nav inNavbar>
                <DropdownToggle nav caret>
                  Подборки
                </DropdownToggle>
                <DropdownMenu right>
                  <DropdownItem>
                    По ценам
                  </DropdownItem>
                  <DropdownItem divider />
                  <DropdownItem>
                    По сезонам
                  </DropdownItem>
                  <DropdownItem divider />
                  <DropdownItem>
                    По типу отдыха
                  </DropdownItem>
                </DropdownMenu>
              </UncontrolledDropdown>
            


            </Nav>
          </Collapse>
        </Navbar>
      </div>
    );
  }
}

export default Navigation;
