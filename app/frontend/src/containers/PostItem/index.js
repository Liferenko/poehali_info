/*
Maintainer: Pavel Liferenko <t.me/Liferenko>
Description: Container with template of post.

TODO:
  - Redux: to add id to payload
  - to add requests' parameters in URL: limit, offset, tag
  - remove a whole axios.get().then() function to ApiHandler component
  -
*/



import React, {Component} from 'react'
import { 
  Image, 
  Card }                  from 'semantic-ui-react'

// Components
import FavoriteStar       from '../../components/FavoriteStar' 
import ReadMore       from '../../components/ReadMore'

// /Components


class PostItem extends Component {
    render() {
        
        const { tour } = this.props


        return (
            <div className="post__item">
              <Card>
                    <Image src={tour.imageUrl} alt="TODO" />
                    <Card.Content>
                        <Card.Header>{tour.city}, {tour.name}</Card.Header>
                        <Card.Meta>
                          <span className='date'>Вылет из {tour.flyFrom} {tour.departureDate}</span>
                        </Card.Meta>
                        <Card.Description>Цена за человека - {tour.USD + ' ' + tour.price} на {tour.nights} ночей.</Card.Description>
                        <Card.Description>{tour.description}</Card.Description>
                      </Card.Content>
                    <Card.Content extra>
                      <Card.Meta alt={ tour.description + tour.keywordsKey1}>
                        <ReadMore 
                            link={tour.id} />
                       {/* <FavoriteStar 
                            postId={tour.id} /> */} 
                      </Card.Meta>
                    </Card.Content>
              </Card>
            </div>
        )
    };
}



export default PostItem
