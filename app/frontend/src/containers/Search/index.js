import _                    from 'lodash'
import React, { Component } from 'react'
//import { Col } from 'reactstrap'
import { Segment, Header, Search, Grid, }    from 'semantic-ui-react'
import axios                from 'axios'

// Components
import { APIURL }           from '../../components/ApiHandler'
// /Components




export default class SearchBlock extends Component {
  constructor(props) {
    super(props)
    this.state = {
      apiUrl: `${APIURL}/tours`,
      jsonFromApi: [],
    }
  }

  componentDidMount() {
    axios
      .get(this.state.apiUrl, { crossdomain: true })
      .then( response => {
        const jsonFromApi = response.data 
        console.log(`search - ${jsonFromApi}`)
        this.setState({ jsonFromApi })
        return jsonFromApi
      })
      .catch( error => {
        console.log('There is no data in URL /api/tours')
      })
  }



  componentWillMount() {
    this.resetComponent()
  }

  resetComponent = () => this.setState({ isLoading: false, results: [], value: '' })

  handleResultSelect = (e, { result }) => this.setState({ value: result });


  handleSearchChange = (e, { value }) => {
    this.setState({ isLoading: true, value })
    setTimeout(() => {
      if (this.state.value.length < 1) return this.resetComponent()

      const re = new RegExp(_.escapeRegExp(this.state.value), 'i')
      const isMatch = result => re.test(result.city)

      const filteredResults = _.reduce(
        this.state.jsonFromApi,
        (memo, data, name) => {
          const results = _.filter(data.results, isMatch)
          if (results.length) memo[name] = { name, results } // eslint-disable-line no-param-reassign
          
          return memo
        },
        {},
      )

      this.setState({
        isLoading: false,
        results: filteredResults,
      })
    }, 300)
  }

  render() {
    const { isLoading, value, results } = this.state
    console.log(`search ${this.state.results}`)

    return (
    
    <Grid>
        <Grid.Column width={8}>
          <Search
	          className="search__input"
            category
            loading={isLoading}
            onResultSelect={this.handleResultSelect}
            onSearchChange={_.debounce(this.handleSearchChange, 500, { leading: true })}
            results={results}
            value={value}
            {...this.props}
          />
        </Grid.Column>
{/*     <Grid.Column width={8} style={{ opacity: 0.2 }}>
          <Segment>
            <Header>State</Header>
            <pre style={{ overflowX: 'auto' }}>{JSON.stringify(this.state.value, null, 2)}</pre>
            <Header>Options</Header>
            <pre style={{ overflowX: 'auto'}}>{JSON.stringify(this.state, null, 2)}</pre>
          </Segment>
        </Grid.Column>*/}
      </Grid>


    )
  }
}









