/*
Maintainer: Pavel Liferenko <t.me/Liferenko>
Description: Feed with infinite scroll pagination and post items displaying. 

TODO:
  - to add ability to use this Feed for different types of cards: posts, employees, filtered posts, comments, etc.
*/

import React, { Component } from 'react'
import axios from 'axios'

// Components
import { APIURL } from '../../components/ApiHandler'
import InfinitePagination from '../../components/InfinitePagination'

// Containers
import PostItem from '../PostItem'




class Feed extends Component {
    constructor(props) {
        super(props)
        this.state = {
            apiUrl: `${APIURL}/tours`,
            jsonFromApi: [],
            
        }
    }

    componentDidMount() {
      axios
        .get(this.state.apiUrl, {
          params: {
            limit: this.state.limit,
            offset: this.state.offset,
          },  
          crossdomain: true
      })
        .then( response => {
          const jsonFromApi = response.data 
          console.log(jsonFromApi)
          this.setState({ jsonFromApi })
        })
        .catch( error => {
          console.log('There is no data in URL /api/tours')
        })  
    } 

    render() {
        return (
            <div className="row feed">
                <InfinitePagination>
                    <div className="ui four doubling stackable cards feed_module">
                    {
                      this.state.jsonFromApi.map(tour => (
                          <PostItem 
                            tour={tour}
                            />
                            )
                        )
                    }
                    </div>
                 </InfinitePagination>
            </div>
        );
    }
}


export default Feed
