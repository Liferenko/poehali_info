import React from 'react';
import ReactDOM from 'react-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
//SemanticUI
import 'semantic-ui-css/semantic.min.css';


import './index.css';
// import App from './App';
import Navigation from './components/nav'
import Header from './components/header';
import Post from './components/post';
import Footer from './components/footer';
import registerServiceWorker from './registerServiceWorker';

ReactDOM.render([<Navigation />, <Post />, <Footer />], document.getElementById('root'));
registerServiceWorker();
