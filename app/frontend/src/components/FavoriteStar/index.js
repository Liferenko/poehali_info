import React, { Component } 	from 'react'
import { connect } 				from 'react-redux'
import { 
	ADD_TO_FAVORITE,
 } from '../../store/actions/types'
import { Button } from 'semantic-ui-react'




 class FavoriteStar extends Component {
 	render() {
 		return(
 			<div className="four wide column">
	 			<Button 
	 				postId={this.props.postId}
	 				icon="star" 
	 				onClick={
	 					(this.props.isFavStarActive < 1) ? this.props.addToFavorite : this.props.undoFavorite
	 				}
	 			/>
	 			<p>{ (this.props.isFavStarActive < 1) ? "В избранное" : "Уже добавлено" }</p>
	 			{ console.log(`postId from favorite star - ${this.props.postId}`) }
 			</div>
 			)
 	}
 }


 export default connect (
	state => ({
		isFavStarActive: state.activeFavStar,
                currentPostId: state.activePost
	}),
	dispatch => ({
		addToFavorite() {
			dispatch({ type: ADD_TO_FAVORITE })
		},
		undoFavorite() {
			dispatch({ type: ADD_TO_FAVORITE })
		},
		
	})
 )(FavoriteStar)
