// Unsplash API
import React from 'react'
import key from '../../databases/configs/api.json'

const api = key.unsplash.key.access 

console.log(api)
console.log(`https://api.unsplash.com/photos/?client_id=${api.key.access}`)
// goal url - https://api.unsplash.com/photos/random?client_id=90c6f152851b26b9a5c473cbaf060e4e3c766bef80da335d42acb3a3ce0fedf7&query=canada
