/*
Maintainer: Pavel Liferenko <t.me/Liferenko>
Description: This handler now looks like one variable for many containers. 
TODO:
	- to move this const APIURL to Redux state save
        - to move a whole axios.get().then() function to this component
	- to make this component a main API controller to whole client-side
*/

export const APIURL = 'http://localhost:3011/api'
