import React from 'react'
import { Dimmer, Loader, Segment } from 'semantic-ui-react'


const LoadMore = () => (
	<div className="ui grid">
		<Segment className="component__loader sixteen wide column">
			<Dimmer active inverted>
				<Loader inverted>Ищём ещё туры...</Loader>
			</Dimmer>
		</Segment>
	</div>

)

export default LoadMore