## Poehali.info - brand new site for old client

Description:
Тема дипломной: 
Разработка второй версии сайта тур.фирмы poehali.info. Главный референс: сделать подтягивание новых туров в таком же виде, как у pinterest.com.
Новые туры будут браться автоматом из внутреннего сервиса франчайзера (api у них нет, сейчас делается ручками девочки-менеджера). 


У сайта будет три роли пользователей: 

- admins (can CRUD posts and edit webpages)
- subscribers (clients with premium rights: can subscribe to filters(regions or budget) )
- guest (anonimous visitors. Can read, like or order tours)



Kanban board of this project you can see [here](https://gitlab.com/Liferenko/poehali_info/boards)

Author/Maintainer - P.Liferenko ([telegram](https://t.me/Liferenko), [email](mailto:liferenko@etlgr.com))
